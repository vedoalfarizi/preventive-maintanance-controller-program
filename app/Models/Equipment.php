<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equipment
 * @package App\Models
 * @version March 1, 2019, 8:19 am UTC
 *
 * @property \App\Models\MainEquipment mainEquipment
 * @property \Illuminate\Database\Eloquent\Collection jobTeams
 * @property \Illuminate\Database\Eloquent\Collection Job
 * @property string name
 * @property string main_equipment_id
 */
class Equipment extends Model
{
    public $table = 'equipments';
    public $timestamps = false;

    public $fillable = [
        'id',
        'name',
        'main_equipment_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string',
        'main_equipment_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mainEquipment()
    {
        return $this->belongsTo(\App\Models\MainEquipment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jobs()
    {
        return $this->hasMany(\App\Models\Job::class);
    }
}
