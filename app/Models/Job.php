<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Job
 * @package App\Models
 * @version March 6, 2019, 3:13 pm +07
 *
 * @property \App\Models\Equipment equipment
 * @property \App\Models\Report report
 * @property \Illuminate\Database\Eloquent\Collection jobTeams
 * @property integer report_id
 * @property string equipment_id
 * @property string work_order
 * @property string realisation
 * @property time start_time
 * @property time finish_time
 * @property string status
 * @property integer user_amount
 * @property integer pgooh_amount
 */
class Job extends Model
{
    public $table = 'jobs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'report_id',
        'equipment_id',
        'work_order',
        'realisation',
        'start_time',
        'finish_time',
        'status',
        'user_amount',
        'pgooh_amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'report_id' => 'integer',
        'equipment_id' => 'string',
        'work_order' => 'string',
        'realisation' => 'string',
        'status' => 'string',
        'user_amount' => 'integer',
        'pgooh_amount' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function equipment()
    {
        return $this->belongsTo(\App\Models\Equipment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function report()
    {
        return $this->belongsTo(\App\Models\Report::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function teams()
    {
        return $this->belongsToMany(\App\Models\Team::class, 'job_teams');
    }
}
