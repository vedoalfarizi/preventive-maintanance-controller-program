<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MainEquipment
 * @package App\Models
 * @version March 1, 2019, 7:58 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection Equipment
 * @property \Illuminate\Database\Eloquent\Collection jobTeams
 * @property \Illuminate\Database\Eloquent\Collection jobs
 * @property \Illuminate\Database\Eloquent\Collection Report
 * @property string name
 */
class MainEquipment extends Model
{
    public $table = 'main_equipments';
    public $timestamps = false;

    public $fillable = [
        'id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'unique:main_equipments',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function equipment()
    {
        return $this->hasMany(\App\Models\Equipment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function reports()
    {
        return $this->hasMany(\App\Models\Report::class);
    }
}
