<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Team
 * @package App\Models
 * @version March 4, 2019, 2:11 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection jobTeams
 * @property \Illuminate\Database\Eloquent\Collection jobs
 * @property string name
 */
class Team extends Model
{
    public $table = 'teams';
    public $timestamps = false;

    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function jobs()
    {
        return $this->belongsToMany(\App\Models\Job::class, 'job_teams');
    }
}
