<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 * @package App\Models
 * @version March 4, 2019, 2:16 am UTC
 *
 * @property \App\Models\MainEquipment mainEquipment
 * @property \Illuminate\Database\Eloquent\Collection jobTeams
 * @property \Illuminate\Database\Eloquent\Collection Job
 * @property string main_equipment_id
 */
class Report extends Model
{
    public $table = 'reports';

    public $fillable = [
        'main_equipment_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'main_equipment_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function mainEquipment()
    {
        return $this->belongsTo(\App\Models\MainEquipment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function jobs()
    {
        return $this->hasMany(\App\Models\Job::class);
    }
}
