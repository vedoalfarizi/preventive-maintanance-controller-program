<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JobTeam
 * @package App\Models
 * @version April 1, 2019, 2:35 pm +07
 *
 * @property \App\Models\Job job
 * @property \App\Models\Team team
 * @property \Illuminate\Database\Eloquent\Collection jobs
 * @property integer team_id
 */
class JobTeam extends Model
{
    public $table = 'job_teams';
    public $timestamps = false;

    public $fillable = [
        'team_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'job_id' => 'integer',
        'team_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function job()
    {
        return $this->belongsTo(\App\Models\Job::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function team()
    {
        return $this->belongsTo(\App\Models\Team::class);
    }
}
