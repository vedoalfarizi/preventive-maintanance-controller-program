<?php

namespace App\Repositories;

use App\Models\Equipment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EquipmentRepository
 * @package App\Repositories
 * @version March 1, 2019, 8:19 am UTC
 *
 * @method Equipment findWithoutFail($id, $columns = ['*'])
 * @method Equipment find($id, $columns = ['*'])
 * @method Equipment first($columns = ['*'])
*/
class EquipmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'main_equipment_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Equipment::class;
    }
}
