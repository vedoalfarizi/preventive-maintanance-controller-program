<?php

namespace App\Repositories;

use App\Models\MainEquipment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class MainEquipmentRepository
 * @package App\Repositories
 * @version March 1, 2019, 7:58 am UTC
 *
 * @method MainEquipment findWithoutFail($id, $columns = ['*'])
 * @method MainEquipment find($id, $columns = ['*'])
 * @method MainEquipment first($columns = ['*'])
*/
class MainEquipmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MainEquipment::class;
    }
}
