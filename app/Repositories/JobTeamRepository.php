<?php

namespace App\Repositories;

use App\Models\JobTeam;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class JobTeamRepository
 * @package App\Repositories
 * @version April 1, 2019, 2:35 pm +07
 *
 * @method JobTeam findWithoutFail($id, $columns = ['*'])
 * @method JobTeam find($id, $columns = ['*'])
 * @method JobTeam first($columns = ['*'])
*/
class JobTeamRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'team_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JobTeam::class;
    }
}
