<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Job;
use Illuminate\Support\Facades\DB;
use App\Models\JobTeam;
use Laracasts\Flash\Flash;
use Barryvdh\DomPDF\Facade as PDF;

class HistoryController extends Controller
{

    public function index()
    {
        $histories = Report::select('main_equipment_id')
        ->rightJoin('jobs', 'jobs.report_id', '=', 'reports.id')
        ->groupBy('main_equipment_id')
        ->get();
        
        $histories->map(function($history){
            $jobs = Job::select(DB::raw(
                'jobs.id,
                equipments.main_equipment_id as mainEquipmentID,
                jobs.equipment_id as equipment,
                DATE(jobs.created_at) as tanggal,
                jobs.work_order as workOrder,
                jobs.realisation as realisation,
                jobs.user_amount as user,
                jobs.pgooh_amount as pgooh,
                jobs.start_time as start,
                jobs.finish_time as finish,
                TIME(jobs.finish_time-jobs.start_time) as result,
                jobs.status'
            ))
            ->leftJoin('equipments', 'equipments.id', '=', 'jobs.equipment_id')
            ->where('equipments.main_equipment_id', $history['main_equipment_id'])
            ->orderBy('tanggal')
            ->orderBy('equipment')
            ->get();

            $jobs->map(function($job){
                $team = JobTeam::where('job_id', $job['id'])->get();
                $nteam = count($team);

                if($nteam <= 0){
                    $job['team'] = "Belum Ada";   
                }else{
                    $job['team'] = $team;   
                }

                $works = explode(',', $job->workOrder);
                $job['workOrder'] = $works;
            });            
            
            $history['jobs'] = $jobs;
        });

        return view('histories.index', compact('histories'));
    }

    public function print(Request $request){
        $input = $request->all();
        $fMainEquip = [
            'main_equipment_id' => 'main_equipment_id',
        ];
        
        $histories = Report::select('main_equipment_id')
        ->rightJoin('jobs', 'jobs.report_id', '=', 'reports.id')
        ->where(function ($query) use ($input, $fMainEquip) {
            foreach ($fMainEquip as $column => $key) {
                $query->when(array_get($input, $key), function ($query, $value) use ($column) {
                    $query->where($column, $value);
                });
            }
        })
        ->groupBy('main_equipment_id')
        ->get();

        $fTanggal = [
            'created_at' => 'tanggal',
        ];

        $histories->map(function($history) use ($input, $fTanggal){
            $jobs = Job::select(DB::raw(
                'jobs.id,
                equipments.main_equipment_id as mainEquipmentID,
                jobs.equipment_id as equipment,
                DATE(jobs.created_at) as tanggal,
                jobs.work_order as workOrder,
                jobs.realisation as realisation,
                jobs.user_amount as user,
                jobs.pgooh_amount as pgooh,
                jobs.start_time as start,
                jobs.finish_time as finish,
                TIME(jobs.finish_time-jobs.start_time) as result,
                jobs.status'
            ))
            ->leftJoin('equipments', 'equipments.id', '=', 'jobs.equipment_id')
            ->where('equipments.main_equipment_id', $history['main_equipment_id'])
            ->where(function ($query) use ($input, $fTanggal) {
                foreach ($fTanggal as $column => $key) {
                    $query->when(array_get($input, $key), function ($query, $value) use ($column) {
                        $query->where($column, 'like', '%'.$value.'%');
                    });
                }
            })
            ->orderBy('tanggal')
            ->orderBy('equipment')
            ->get();

            $jobs->map(function($job){
                $team = JobTeam::where('job_id', $job['id'])->get();
                $nteam = count($team);

                if($nteam <= 0){
                    $job['team'] = "Belum Ada";   
                }else{
                    $job['team'] = $team;   
                }

                $works = explode(',', $job->workOrder);
                $job['workOrder'] = $works;
            });            
            
            $history['jobs'] = $jobs;
        });

        return view('histories.print', compact('histories'));
        
        // $pdf = PDF::loadView('histories.print', compact('histories'));
        // $pdf->setPaper('A4', 'landscape');

        // return $pdf->download('report.pdf');
    }
}
