<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMainEquipmentRequest;
use App\Http\Requests\UpdateMainEquipmentRequest;
use App\Models\Equipment;
use App\Models\MainEquipment;
use App\Models\Report;
use App\Repositories\MainEquipmentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MainEquipmentController extends AppBaseController
{
    /** @var  MainEquipmentRepository */
    private $mainEquipmentRepository;

    public function __construct(MainEquipmentRepository $mainEquipmentRepo)
    {
        $this->mainEquipmentRepository = $mainEquipmentRepo;
    }

    /**
     * Display a listing of the MainEquipment.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->mainEquipmentRepository->pushCriteria(new RequestCriteria($request));
        $mainEquipments = $this->mainEquipmentRepository->orderBy('id', 'asc')->all();

        $main_equipment_option = MainEquipment::all()->pluck('name', 'id');
        $main_equipment_option->prepend('Please Select');

        return view('main_equipments.index', compact('mainEquipments','main_equipment_option'));
    }

    /**
     * Show the form for creating a new MainEquipment.
     *
     * @return Response
     */
    public function create()
    {
        return view('main_equipments.create');
    }

    /**
     * Store a newly created MainEquipment in storage.
     *
     * @param CreateMainEquipmentRequest $request
     *
     * @return Response
     */
    public function store(CreateMainEquipmentRequest $request)
    {
        $input = $request->all();

        $mainEquipment = $this->mainEquipmentRepository->create($input);

        Flash::success('Main Equipment saved successfully.');

        return redirect(route('mainEquipments.index'));
    }

    /**
     * Display the specified MainEquipment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mainEquipment = $this->mainEquipmentRepository->findWithoutFail($id);

        if (empty($mainEquipment)) {
            Flash::error('Main Equipment not found');

            return redirect(route('mainEquipments.index'));
        }

        $equipments = Equipment::where('main_equipment_id', $id)->orderBy('id', 'asc')->get();
        $mainEquipmentName = MainEquipment::where('id', $id)->first(['name']);

        return view('main_equipments.show', compact('equipments', 'mainEquipmentName'));
    }

    /**
     * Show the form for editing the specified MainEquipment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mainEquipment = $this->mainEquipmentRepository->findWithoutFail($id);

        if (empty($mainEquipment)) {
            Flash::error('Main Equipment not found');

            return redirect(route('mainEquipments.index'));
        }

        return view('main_equipments.edit')->with('mainEquipment', $mainEquipment);
    }

    /**
     * Update the specified MainEquipment in storage.
     *
     * @param  int              $id
     * @param UpdateMainEquipmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMainEquipmentRequest $request)
    {
        $mainEquipment = $this->mainEquipmentRepository->findWithoutFail($id);

        if (empty($mainEquipment)) {
            Flash::error('Main Equipment not found');

            return redirect(route('mainEquipments.index'));
        }

        $mainEquipment = $this->mainEquipmentRepository->update($request->all(), $id);

        Flash::success('Main Equipment updated successfully.');

        return redirect(route('mainEquipments.index'));
    }

    /**
     * Remove the specified MainEquipment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mainEquipment = $this->mainEquipmentRepository->findWithoutFail($id);

        if (empty($mainEquipment)) {
            Flash::error('Main Equipment not found');

            return redirect(route('mainEquipments.index'));
        }

        $reports = Report::where('main_equipment_id', $id)->count();

        if ($reports > 0) {
            Flash::error('Masih ada data report yang menggunakan Main Equipment ini');

            return redirect(route('mainEquipments.index'));
        }

        $equipments = Equipment::where('main_equipment_id', $id)->count();

        if ($equipments > 0) {
            Flash::error('Masih ada data equipment yang menggunakan Main Equipment ini');

            return redirect(route('mainEquipments.index'));
        }

        $this->mainEquipmentRepository->delete($id);

        Flash::success('Main Equipment deleted successfully.');

        return redirect(route('mainEquipments.index'));
    }
}
