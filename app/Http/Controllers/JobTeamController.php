<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobTeamRequest;
use App\Http\Requests\UpdateJobTeamRequest;
use App\Repositories\JobTeamRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class JobTeamController extends AppBaseController
{
    /** @var  JobTeamRepository */
    private $jobTeamRepository;

    public function __construct(JobTeamRepository $jobTeamRepo)
    {
        $this->jobTeamRepository = $jobTeamRepo;
    }

    /**
     * Display a listing of the JobTeam.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Show the form for creating a new JobTeam.
     *
     * @return Response
     */
    public function create()
    {
        return redirect(route('reports.index'));
    }

    /**
     * Store a newly created JobTeam in storage.
     *
     * @param CreateJobTeamRequest $request
     *
     * @return Response
     */
    public function store(CreateJobTeamRequest $request)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Display the specified JobTeam.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Show the form for editing the specified JobTeam.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Update the specified JobTeam in storage.
     *
     * @param  int              $id
     * @param UpdateJobTeamRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobTeamRequest $request)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Remove the specified JobTeam from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        return redirect(route('reports.index'));
    }
}
