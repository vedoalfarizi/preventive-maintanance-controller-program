<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\UpdateReportRequest;
use App\Models\Equipment;
use App\Models\Job;
use App\Models\MainEquipment;
use App\Models\Report;
use App\Repositories\ReportRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReportController extends AppBaseController
{
    /** @var  ReportRepository */
    private $reportRepository;

    public function __construct(ReportRepository $reportRepo)
    {
        $this->reportRepository = $reportRepo;
    }

    /**
     * Display a listing of the Report.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $to = date('Y-m-d H:i:s');
        $from = date('Y-m-d H:i:s', strtotime("-1 days"));
        $this->reportRepository->pushCriteria(new RequestCriteria($request));

        $reports = Report::whereBetween('created_at', [$from, $to])->get();

        return view('reports.index', compact('reports'));
    }

    /**
     * Show the form for creating a new Report.
     *
     * @return Response
     */
    public function create()
    {
        $mainEquipmentOption = MainEquipment::orderBy('id','asc')->get()->pluck('name', 'id', 'name');
        
        return view('reports.create', compact('mainEquipmentOption'));
    }

    /**
     * Store a newly created Report in storage.
     *
     * @param CreateReportRequest $request
     *
     * @return Response
     */
    public function store(CreateReportRequest $request)
    {
        $input = $request->all();

        $report = $this->reportRepository->create($input);

        Flash::success('Data saved successfully.');

        return redirect(route('reports.index'));
    }

    /**
     * Display the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Show the form for editing the specified Report.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Update the specified Report in storage.
     *
     * @param  int              $id
     * @param UpdateReportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReportRequest $request)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Remove the specified Report from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);

        if (empty($report)) {
            Flash::error('Report not found');

            return redirect(route('reports.index'));
        }

        $jobs = Job::where('report_id', $id)->count();

        if ($jobs > 0) {
            Flash::error('Report ini masih memiliki data job');

            return redirect(route('reports.index'));
        }

        $this->reportRepository->delete($id);

        Flash::success('Report deleted successfully.');

        return redirect(route('reports.index'));
    }

    public function autoMainEquipData(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $autoMainEquip = $request->q;
            $data = DB::table('main_equipments')->select('id', 'name')->where('id', 'LIKE', "%$autoMainEquip%")->get();
        }

        return response()->json($data);
    }

    public function autoEquipData(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $autoMainEquip = $request->q;
            $mainEquipId = $request->mainEquipId;
            $data = DB::table('equipments')->select('id', 'name')->where('id', 'LIKE', "%$autoMainEquip%")
                ->where('main_equipment_id', '=', $mainEquipId)->get();
        }

        return response()->json($data);
    }


}
