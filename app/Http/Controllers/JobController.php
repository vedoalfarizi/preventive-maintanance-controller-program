<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJobRequest;
use App\Http\Requests\UpdateJobRequest;
use App\Models\Job;
use App\Models\Report;
use App\Repositories\JobRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Symfony\Component\Console\Tests\Output\TestOutput;
use App\Models\JobTeam;
use Illuminate\Support\Facades\DB;

class JobController extends AppBaseController
{
    /** @var  JobRepository */
    private $jobRepository;

    public function __construct(JobRepository $jobRepo)
    {
        $this->jobRepository = $jobRepo;
    }

    /**
     * Display a listing of the Job.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        return redirect(route('reports.index'));
    }

    /**
     * Show the form for creating a new Job.
     *
     * @return Response
     */
    public function create()
    {
        return redirect(route('reports.index'));
    }

    /**
     * Store a newly created Job in storage.
     *
     * @param CreateJobRequest $request
     *
     * @return Response
     */
    public function store(CreateJobRequest $request)
    {
        $input = $request->all();
        $reportID = $input['report_id'];

        $job = $this->jobRepository->create($input);

        Flash::success('Job saved successfully.');

        return redirect(route('jobs.show', [$reportID]));
    }

    /**
     * Display the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobs = Job::where('report_id', $id)->get();
        
        $jobs->map(function($job){
            $team = JobTeam::where('job_id', $job['id'])->get();
            $nteam = count($team);

            if($nteam <= 0){
                $job['team'] = "Belum Ada";   
            }else{
                $team = JobTeam::where('job_id', $job['id'])->get();
                $job['team'] = $team;   
            }
        });

        $mainEquipId = Report::where('id', $id)->first(['main_equipment_id']);
        $mainEquipName = $mainEquipId->main_equipment_id .'(' .$mainEquipId->mainEquipment->name.')';
        $reportID = $id;
        return view('reports.job_table', compact('jobs', 'mainEquipName', 'reportID'));
    }

    /**
     * Show the form for editing the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $update = true;
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.edit', compact('job', 'update'));
    }

    /**
     * Update the specified Job in storage.
     *
     * @param  int              $id
     * @param UpdateJobRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobRequest $request)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        $job = $this->jobRepository->update($request->all(), $id);
        $reportID = $job->report_id;

        Flash::success('Job updated successfully.');

        return redirect(route('jobs.show', [$reportID]));
    }

    public function createTeam(Request $request)
    {
        $input = $request->all();
        $reportID = $input['report_id'];
        $nTeam = count($input['name']);

        for ($i=0; $i < $nTeam; $i++) { 
            DB::table('job_teams')->insert(
                [
                    'job_id' => $input['job_id'], 
                    'team_id' => $input['name'][$i]
                ]
            );
        }

        Flash::success('Team saved successfully.');

        return redirect(route('jobs.show', [$reportID]));
    }

    public function updateTeam(Request $request)
    {
        $input = $request->all();
        $reportID = $input['report_id'];

        DB::table('job_teams')->where('job_id', $input['job_id'])->delete();
        
        $nTeam = count($input['name']);
        for ($i=0; $i < $nTeam; $i++) { 
            DB::table('job_teams')->insert(
                [
                    'job_id' => $input['job_id'], 
                    'team_id' => $input['name'][$i]
                ]
            );
        }

        Flash::success('Team saved successfully.');

        return redirect(route('jobs.show', [$reportID]));
    }

    /**
     * Remove the specified Job from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        $reportID = $job->report_id;

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.show', [$reportID]));
        }

        $jobTeam = JobTeam::where('job_id', $id)->count();
        
        if ($jobTeam > 0) {
            Flash::error('Job ini masih terdaftar di daftar pekerjaan sebuah tim');

            return redirect(route('jobs.show', [$reportID]));
        }

        $this->jobRepository->delete($id);

        Flash::success('Job deleted successfully.');

        return redirect(route('jobs.show', [$reportID]));
    }
}
