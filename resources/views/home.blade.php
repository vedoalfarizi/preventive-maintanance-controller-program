@extends('layouts.app')

@section('content')
<section class="content-header">
</section>

<div class="container">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-cogs"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Main Equipments</span>
                    <span class="info-box-number">{{$nMain}}</span>
                    </div>
                </div>    
            </div>
                
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-cog"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Equipments</span>
                        <span class="info-box-number">{{$nEquip}}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-ios-body"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Teams</span>
                        <span class="info-box-number">{{$nTeam}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection