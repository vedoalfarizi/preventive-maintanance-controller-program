<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{!! route('home') !!}"><i class="fa fa-edit"></i><span>Dashboard</span></a>
</li>

<li class="{{ Request::is('reports*') ? 'active' : '' || Request::is('jobs*') ? 'active' : ''}}">
    <a href="{!! route('reports.index') !!}"><i class="fa fa-edit"></i><span>Input Job</span></a>
</li>

<li class="{{ Request::is('mainEquipments*') ? 'active' : '' || Request::is('equipment*') ? 'active' : '' }}">
    <a href="{!! route('mainEquipments.index') !!}"><i class="fa fa-edit"></i><span>Main Equipments</span></a>
</li>

<li class="{{ Request::is('teams*') ? 'active' : '' }}">
    <a href="{!! route('teams.index') !!}"><i class="fa fa-edit"></i><span>Personel</span></a>
</li>

<li class="{{ Request::is('histories*') ? 'active' : '' }}">
    <a href="{!! route('histories.index') !!}"><i class="fa fa-edit"></i><span>Reporting</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>User Accounts</span></a>
</li>