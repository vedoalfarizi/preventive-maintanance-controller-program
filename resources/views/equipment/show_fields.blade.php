<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $equipment->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $equipment->name !!}</p>
</div>

<!-- Main Equipment Id Field -->
<div class="form-group">
    {!! Form::label('main_equipment_id', 'Main Equipment Id:') !!}
    <p>{!! $equipment->main_equipment_id !!}</p>
</div>

