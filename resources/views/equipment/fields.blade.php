<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Code:') !!}
    {!! Form::text('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

@if(isset($edit_mode))
        {!! Form::hidden('main_equipment_id', $mainEquipId, ['class' => 'form-control']) !!}
@else
        {!! Form::hidden('main_equipment_id', $mainEquipment->id, ['class' => 'form-control']) !!}
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(isset($edit_mode))
        <a href="{!! route('mainEquipments.show', [$mainEquipId]) !!}" class="btn btn-default">Cancel</a>
    @else
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    @endif
</div>
