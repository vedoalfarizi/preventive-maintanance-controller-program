<table class="table table-responsive" id="equipment-table">
    <thead>
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($equipments as $equipment)
        <tr>
            <td>{!! $equipment->id !!}</td>
            <td>{!! $equipment->name !!}</td>
            <td>
                {!! Form::open(['route' => ['equipment.destroy', $equipment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('equipment.edit', [$equipment->id]) !!}" class='btn btn-default btn-xs'>Edit</a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>