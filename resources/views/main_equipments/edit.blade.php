@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Main Equipment
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mainEquipment, ['route' => ['mainEquipments.update', $mainEquipment->id], 'method' => 'patch']) !!}

                        @include('main_equipments.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection