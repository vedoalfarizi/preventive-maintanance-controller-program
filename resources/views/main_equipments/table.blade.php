<table class="table table-responsive" id="mainEquipments-table">
    <thead>
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th colspan="4">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($mainEquipments as $mainEquipment)
        <tr>
            <td>{!! $mainEquipment->id !!}</td>
            <td>{!! $mainEquipment->name !!}</td>
            <td>
                {!! Form::open(['route' => ['mainEquipments.destroy', $mainEquipment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('Tambah Equipment', ['type' => 'button', 'class' => 'btn btn-info btn-xs', 'data-toggle' => "modal", 'data-target' => "#equipModal-$mainEquipment->id"]) !!}
                    <a href="{!! route('mainEquipments.show', [$mainEquipment->id]) !!}" class='btn btn-default btn-xs'>Lihat Equipment</a>
                    <a href="{!! route('mainEquipments.edit', [$mainEquipment->id]) !!}" class='btn btn-default btn-xs'>Edit</a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>

        <!-- Modal -->
        <div id="equipModal-{!! $mainEquipment->id !!}" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Add New Equipment</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['route' => 'equipment.store']) !!}

                        @include('equipment.fields')

                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>

            </div>
        </div>
    @endforeach
    </tbody>
</table>