@foreach($histories as $history)
        <h4>Main Equipment: {{$history->mainEquipment->name}}</h4>

        <table class="table table-responsive" id="reports-table">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Tanggal</th>
                    <th rowspan="2">Equipment</th>
                    <th colspan="2" style="text-align:center">Job Activity</th>
                    <th rowspan="2">Team PGO</th>
                    <th colspan="2" style="text-align:center">Man Power</th>
                    <th colspan="3" style="text-align:center">Work Time</th>
                    <th rowspan="2">Status</th>
                </tr>
                <tr>
                    <th>Work Order</th>
                    <th>Realisasi</th>
                    <th>User</th>
                    <th>PGO-OH</th>
                    <th>ST</th>
                    <th>FT</th>
                    <th>RT</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no=1;
                    $currentTanggal = null;
                @endphp
                @foreach ($history->jobs as $job)
                <tr>
                    @if ($currentTanggal !== $job->tanggal)
                        <td>{{$no}}</td>
                        <td>{{$job->tanggal}}</td>
                        @php
                            $currentTanggal = $job->tanggal;
                            $no++;
                        @endphp    
                    @else()
                        <td></td>
                        <td></td>
                    @endif
                    <td>{{$job->equipment}}</td>
                    <td>
                        <ol>
                            @foreach ($job->workOrder as $work)
                                <li>{{$work}}</li>
                            @endforeach
                        </ol>
                    </td>
                    <td>
                        @if ($job->realisation === NULL)
                            Belum Ada
                        @else
                            @php
                                $reals = explode(',', $job->realisation);
                            @endphp
                            <ol>
                                @foreach ($reals as $real)
                                    <li>{{$real}}</li>
                                @endforeach
                            </ol>
                        @endif
                    </td>
                    <td>
                        <ol>
                            @if ($job->team !== 'Belum Ada')
                                <ol>
                                    @foreach ($job->team as $team)
                                        <li>{{$team->team->name}}</li>
                                    @endforeach
                                </ol>
                            @else
                                {{$job->team}}
                            @endif
                        </ol>
                    </td>
                    <td>{{$job->user}}</td>
                    <td>{{$job->pgooh}}</td>
                    <td>{{$job->start}}</td>
                    <td>{{$job->finish}}</td>
                    <td>{{$job->result}}</td>
                    <td>{{$job->status}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>  
        
        <br><hr style="height:1px;border:none;color:#333;background-color:#333;">

@endforeach