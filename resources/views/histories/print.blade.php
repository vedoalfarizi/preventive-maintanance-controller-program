<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PMC Semen Padang</title>
</head>
<body>
    <style>
        @media all {
                            
            @page { margin: 2cm; size: A4 landscape; }
            
            body {
            width: 100%; 
            margin: 0; 
            float: none;
            font: 11pt Georgia, "Times New Roman", Times, serif;
            line-height: 1.3;
            background: #fff !important;
            color: #000;
            }
            
            h4 {
            font-size: 14pt;
            margin-top: 25px;
            }    
            
            h4 { page-break-after:avoid; 
                page-break-inside:avoid }
            table, pre { page-break-inside:avoid }
            ul, ol, dl  { page-break-before:avoid }

            table {
                table-layout: fixed;
                width: 100%;
                border-collapse: collapse;
            }

            thead th:nth-child(1) {
               width: 2%;
            }

            thead th:nth-child(2) {
               width: 9%;
            }

            thead th:nth-child(3) {
               width: 9%;
            }
            
            thead th:nth-child(4) {
               width: 30%;
            }

            thead th:nth-child(5) {
               width: 12%;
            }

            thead th:nth-child(6) {
               width: 13%;
            }

            thead th:nth-child(7) {
               width: 20%;
            }

            thead th:nth-child(8) {
               width: 5%;
            }
            

            th,td {
                padding: 4px 4px 4px 4px ;
                text-align: center ;
            }
            th {
                border-bottom: 2px solid #333333 ;
            }
            td {
                border: 1px dotted #999999 ;
            }
            tfoot td {
                border-bottom-width: 0px ;
                border-top: 2px solid #333333 ;
                padding-top: 20px ;
            }
            
            p, address, li, dt, dd, blockquote {
            font-size: 100%
            }
            
            li {
            line-height: 1.6em;
            }    
                
        }
    </style>

    @foreach($histories as $history)
        <h4>Main Equipment: {{$history->mainEquipment->name}}</h4>

        <table cellspacing="0">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">Tanggal</th>
                    <th rowspan="2">Equipment</th>
                    <th colspan="2" style="text-align:center">Job Activity</th>
                    <th rowspan="2">Team PGO</th>
                    <th colspan="2" style="text-align:center">Man Power</th>
                    <th colspan="3" style="text-align:center">Work Time</th>
                    <th rowspan="2">Status</th>
                </tr>
                <tr>
                    <th>Work Order</th>
                    <th>Realisasi</th>
                    <th>User</th>
                    <th>PGO-OH</th>
                    <th>ST</th>
                    <th>FT</th>
                    <th>RT</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $no=1;
                    $currentTanggal = null;
                @endphp
                @foreach ($history->jobs as $job)
                <tr>
                    @if ($currentTanggal !== $job->tanggal)
                        <td>{{$no}}</td>
                        <td>{{$job->tanggal}}</td>
                        @php
                            $currentTanggal = $job->tanggal;
                            $no++;
                        @endphp    
                    @else()
                        <td></td>
                        <td></td>
                    @endif
                    <td>{{$job->equipment}}</td>
                    <td>
                        <ol>
                            @foreach ($job->workOrder as $work)
                                <li>{{$work}}</li>
                            @endforeach
                        </ol>
                    </td>
                    <td>
                        @if ($job->realisation === NULL)
                            Belum Ada
                        @else
                            @php
                                $reals = explode(',', $job->realisation);
                            @endphp
                            <ol>
                                @foreach ($reals as $real)
                                    <li>{{$real}}</li>
                                @endforeach
                            </ol>
                        @endif
                    </td>
                    <td>
                        @if ($job->team !== 'Belum Ada')
                            <ol>
                                @foreach ($job->team as $team)
                                    <li>{{$team->team->name}}</li>
                                @endforeach
                            </ol>
                        @else
                            {{$job->team}}
                        @endif
                    </td>
                    <td>{{$job->user}}</td>
                    <td>{{$job->pgooh}}</td>
                    <td>{{$job->start}}</td>
                    <td>{{$job->finish}}</td>
                    <td>{{$job->result}}</td>
                    <td>{{$job->status}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>  
        
        <br><hr style="height:1px;border:none;color:#333;background-color:#333;">

    @endforeach
</body>
</html>