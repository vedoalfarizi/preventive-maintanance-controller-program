@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">All Time Reporting</h1>
        <h1 class="pull-right">
                <a id="printBtn" href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#print"><i class="glyphicon glyphicon-print"></i></a>
        </h1>

        <div id="print" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Make Report</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['route' => 'histories.print', 'target' => '_blank']) !!}
                        
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    {!! Form::label('tanggal', 'Tanggal:') !!}
                                    {!! Form::date('tanggal', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group col-sm-6">
                                    {!! Form::label('main_equipment_id', 'Main Equipment Id:') !!}
                                    <br>
                                    <select class="main_equipment_id form-control" name="main_equipment_id" style="width:100%">
                                        <option value="0" selected>--Main Equipment--</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-sm-6">
                                {!! Form::submit('Print', ['class' => 'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('histories.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>

    <script type="text/javascript">
        $('.main_equipment_id').select2({
            placeholder: 'Type Main Equipment Code ...',
            ajax: {
                url: '{!! url('/autoMainEquipData') !!}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.id+' - '+item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    </script>    
@endsection

