<!-- Name Field -->
<div class="form-group col-sm-12">
        {!! Form::label('name', 'Name:') !!}
        <select style="width: 100%" class="team_name form-control" multiple="multiple" name="name[]"></select>
        <input type="hidden" name="job_id" value="{{$job->id}}"/>
        <input type="hidden" name="report_id" value="{{$reportID}}"/>
    </div>
    
    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        @if(isset($edit))
            <a href="{!! route('teams.index') !!}" class="btn btn-default">Cancel</a>
        @else
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        @endif
    </div>
    
    <script type="text/javascript">
        $('.team_name').select2({
            placeholder: 'Type Name ...',
            ajax: {
                url: '{!! url('/getTeamName') !!}',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    </script>