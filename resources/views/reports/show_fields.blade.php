<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $report->id !!}</p>
</div>

<!-- Main Equipment Id Field -->
<div class="form-group">
    {!! Form::label('main_equipment_id', 'Main Equipment Id:') !!}
    <p>{!! $report->main_equipment_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $report->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $report->updated_at !!}</p>
</div>

