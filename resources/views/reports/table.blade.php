<table class="table table-responsive" id="reports-table">
    <thead>
        <tr>
            <th>Main Equipment</th>
            <th>Created At</th>
            <th>Updated At</th>
            <th>Jobs</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td>{!! $report->main_equipment_id !!} - {!! $report->mainEquipment->name !!}</td>
            <td>{!! $report->created_at->format('l, F Y H:i:s') !!}</td>
            <td>{!! $report->updated_at->format('l, F Y H:i:s') !!}</td>
            <td>
                <div class='btn-group'>
                    <a href="{!! route('jobs.show', [$report->id]) !!}" class='btn btn-default btn-xs'>Lihat Daftar</a>
                    <a id="jobBtn" href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#add-job-{!! $report->id !!}">Tambah</a>
                </div>
            </td>
            <td>
                {!! Form::open(['route' => ['reports.destroy', $report->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>

        <div id="add-job-{!! $report->id !!}" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tambah Pekerjaan untuk {!! $report->main_equipment_id !!} - {!! $report->mainEquipment->name !!}</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open(['route' => 'jobs.store']) !!}
                            @include('jobs.fields')
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>

            </div>
        </div>
    @endforeach
    </tbody>
</table>

<script type="text/javascript">
    $('#jobBtn').on('click', function (e) {
        var mainEquipmentId = $('#main_equipment_id').val();

        $('.equipment_id').select2({
            placeholder: 'Type Equipment Code ...',
            ajax: {
                url: '{!! url('/autoEquipData') !!}',
                dataType: 'json',
                type:'GET',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        mainEquipId: mainEquipmentId
                    };
                },
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.id+' - '+item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    });
</script>
