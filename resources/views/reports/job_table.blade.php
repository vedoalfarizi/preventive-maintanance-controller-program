@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Daftar Pekerjaan {!! $mainEquipName !!}</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('reports.index') !!}">Kembali ke Daftar Lokasi Pekerjaan</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">

                <table class="table table-responsive" id="jobs-table">
                    <thead>
                        <tr>
                            <th>Equipment</th>
                            <th>Work Order</th>
                            <th>Realisation</th>
                            <th>Team PGO</th>
                            <th>Status</th>
                            <th>User</th>
                            <th>PGOH</th>
                            <th>Start</th>
                            <th>Finish</th>
                            <th>Result</th>
                            <th colspan="3">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($jobs as $job)
                        <tr>
                            <td>{!! $job->equipment_id !!}<br>({!! $job->equipment->name !!})</td>
                            <td>
                                @php
                                    $workArray = explode(',', $job->work_order);
                                @endphp
                                <ol>
                                    @foreach($workArray as $work)
                                        <li>{!! $work !!}</li>
                                    @endforeach
                                </ol>
                            </td>
                            <td>@if($job->realisation != "" || $job->realisation != null)
                                    @php
                                        $realArray = explode(',', $job->realisation);
                                    @endphp
                                    <ol>
                                        @foreach($realArray as $real)
                                            <li>{!! $real !!}</li>
                                        @endforeach
                                    </ol>
                                @else
                                    Belum Ada
                                @endif
                            </td>
                            <td>
                                @if($job->team === 'Belum Ada')
                                    {{$job->team}}
                                @else
                                    @foreach($job->team as $team)
                                        {{$team->team->name}} <br>
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                @if($job->status == 'F')
                                    Failed
                                @elseif($job->status == 'C')
                                    Completed
                                @elseif($job->status == 'P')
                                    Pending
                                @else
                                    Belum Diset
                                @endif
                            </td>
                            <td>{!! $job->user_amount !!}</td>
                            <td>{!! $job->pgooh_amount !!}</td>
                            <td>{!! $job->start_time !!}</td>
                            <td>
                                @if($job->finish_time != null)
                                    {!! $job->finish_time !!}
                                @else
                                    00:00:00
                                @endif
                            </td>
                            <td>
                                @if($job->finish_time != null)
                                    @php
                                        $timeFinish = $job->finish_time;
                                        $timeStart = $job->start_time;
                                        $interval = intval((strtotime($timeFinish) - strtotime($timeStart))/60);

                                        $hours = intval($interval/60);
                                        $minutes = $interval%60;
                                    @endphp
                                    {!! $hours !!}:{!! $minutes !!}
                                @else
                                    00:00:00
                                @endif
                            </td>
                            <td>
                                {!! Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    @if($job->team === 'Belum Ada')
                                        <a id="teamBtn" href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#add-team-{!! $job->id !!}">Tambah Personel</a>
                                    @else
                                        <a id="teamBtnUpdate" href="" class="btn btn-success btn-xs" data-toggle="modal" data-target="#update-team-{!! $job->id !!}">Update Personel</a>
                                    @endif
                                    <a href="{!! route('jobs.edit', [$job->id]) !!}" class='btn btn-default btn-xs'>Realisasi</a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>

                        <div id="add-team-{!! $job->id !!}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Personel</h4>
                                    </div>
                                    <div class="modal-body">
                                        {!! Form::open(['route' => 'jobs.createTeam']) !!}
                                            @include('reports.team_fields')
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="update-team-{!! $job->id !!}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Personel</h4>
                                        </div>
                                        <div class="modal-body">
                                            {!! Form::open(['route' => 'jobs.updateTeam']) !!}
                                                @include('reports.team_fields')
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">
        </div>
    </div>
@endsection