<!-- Main Equipment Id Field -->
<div class="container form-group col-sm-6">
    {!! Form::label('main_equipment_id', 'Main Equipment Id:') !!}
    <select class="main_equipment_id form-control" name="main_equipment_id"></select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('reports.index') !!}" class="btn btn-default">Cancel</a>
</div>

<script type="text/javascript">
    $('.main_equipment_id').select2({
        placeholder: 'Type Main Equipment Code ...',
        ajax: {
            url: '{!! url('/autoMainEquipData') !!}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        return {
                            text: item.id+' - '+item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
