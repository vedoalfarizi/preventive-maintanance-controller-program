@if(isset($update))
    {!! Form::hidden('report_id', $job->report_id, ['class' => 'form-control']) !!}
    {!! Form::hidden('main_equipment_id', $job->equipment->main_equipment_id, ['id' => 'main_equipment_id','class' => 'form-control']) !!}
    {!! Form::hidden('equipment_id', $job->equipment_id, ['class' => 'form-control']) !!}

    <!-- Work Order Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('work_order', 'Work Order:') !!}
        {!! Form::textarea('work_order', null, ['class' => 'form-control', 'disabled' => 'true']) !!}
    </div>

    <!-- Realisation Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('realisation', 'Realisation:') !!}
        {!! Form::textarea('realisation', null, ['class' => 'form-control', 'placeholder' => 'Pisahkan dengan setiap realisasi dengan tanda koma(,)']) !!}
    </div>

    <!-- Finish Time Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('finish_time', 'Finish Time:') !!}
        {!! Form::time('finish_time', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Status Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('status', 'Status:') !!}
        <select name="status" class="form-control">
            <option value="F" {{ $job->status == 'f' ? 'selected' : '' }}>Failed</option>
            <option value="C" {{ $job->status == 'c' ? 'selected' : '' }}>Completed</option>
            <option value="P" {{ $job->status == 'p' ? 'selected' : '' }}>Pending</option>
        </select>
    </div>
@else
    {!! Form::hidden('report_id', $report->id, ['class' => 'form-control']) !!}

    <!-- Equipment Id Field -->
    <div class="form-group col-sm-12">
        {!! Form::label('equipment_id', 'Equipment Code:') !!}
        {!! Form::hidden('main_equipment_id', $report->main_equipment_id, ['id' => 'main_equipment_id','class' => 'form-control']) !!}
        <select class="equipment_id form-control" name="equipment_id" style="width: 100%"></select>
    </div>

    <!-- Work Order Field -->
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('work_order', 'Work Order:') !!}
        {!! Form::textarea('work_order', null, ['class' => 'form-control', 'placeholder' => 'Pisahkan dengan setiap pekerjaan dengan tanda koma(,)']) !!}
    </div>

    <!-- Start Time Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('start_time', 'Start Time:') !!}
        {!! Form::time('start_time', null, ['class' => 'form-control']) !!}
    </div>
@endif

<!-- User Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_amount', 'User Amount:') !!}
    {!! Form::number('user_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Pgooh Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pgooh_amount', 'PGOH Amount:') !!}
    {!! Form::number('pgooh_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(isset($update))
        <a href="{!! route('jobs.show', [$job->report_id]) !!}" class="btn btn-default">Cancel</a>
    @else
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    @endif
</div>