<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $job->id !!}</p>
</div>

<!-- Report Id Field -->
<div class="form-group">
    {!! Form::label('report_id', 'Report Id:') !!}
    <p>{!! $job->report_id !!}</p>
</div>

<!-- Equipment Id Field -->
<div class="form-group">
    {!! Form::label('equipment_id', 'Equipment Id:') !!}
    <p>{!! $job->equipment_id !!}</p>
</div>

<!-- Work Order Field -->
<div class="form-group">
    {!! Form::label('work_order', 'Work Order:') !!}
    <p>{!! $job->work_order !!}</p>
</div>

<!-- Realisation Field -->
<div class="form-group">
    {!! Form::label('realisation', 'Realisation:') !!}
    <p>{!! $job->realisation !!}</p>
</div>

<!-- Start Time Field -->
<div class="form-group">
    {!! Form::label('start_time', 'Start Time:') !!}
    <p>{!! $job->start_time !!}</p>
</div>

<!-- Finish Time Field -->
<div class="form-group">
    {!! Form::label('finish_time', 'Finish Time:') !!}
    <p>{!! $job->finish_time !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $job->status !!}</p>
</div>

<!-- User Amount Field -->
<div class="form-group">
    {!! Form::label('user_amount', 'User Amount:') !!}
    <p>{!! $job->user_amount !!}</p>
</div>

<!-- Pgooh Amount Field -->
<div class="form-group">
    {!! Form::label('pgooh_amount', 'Pgooh Amount:') !!}
    <p>{!! $job->pgooh_amount !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $job->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $job->updated_at !!}</p>
</div>

