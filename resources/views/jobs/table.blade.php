<table class="table table-responsive" id="jobs-table">
    <thead>
        <tr>
            <th>Report Id</th>
        <th>Equipment Id</th>
        <th>Work Order</th>
        <th>Realisation</th>
        <th>Start Time</th>
        <th>Finish Time</th>
        <th>Status</th>
        <th>User Amount</th>
        <th>Pgooh Amount</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($jobs as $job)
        <tr>
            <td>{!! $job->report_id !!}</td>
            <td>{!! $job->equipment_id !!}</td>
            <td>{!! $job->work_order !!}</td>
            <td>{!! $job->realisation !!}</td>
            <td>{!! $job->start_time !!}</td>
            <td>{!! $job->finish_time !!}</td>
            <td>{!! $job->status !!}</td>
            <td>{!! $job->user_amount !!}</td>
            <td>{!! $job->pgooh_amount !!}</td>
            <td>
                {!! Form::open(['route' => ['jobs.destroy', $job->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('jobs.show', [$job->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('jobs.edit', [$job->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>