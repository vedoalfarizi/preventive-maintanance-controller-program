<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Models\Equipment::class, function (Faker\Generator $faker) {
    static $main_equipment_id;

    return [
        'id' => str_random(6),
        'name' => $faker->name,
        'main_equipment_id' => $main_equipment_id = 'D1eqr7',
    ];
});

