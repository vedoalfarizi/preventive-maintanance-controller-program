<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('users', 'UserController');
    Route::post('users/{user}/reset-password', 'UserController@reset')->name('users.reset');

    Route::resource('mainEquipments', 'MainEquipmentController');

    Route::resource('equipment', 'EquipmentController');

    Route::resource('teams', 'TeamController');
    Route::get('/getTeamName', 'TeamController@getTeamName');

    Route::resource('reports', 'ReportController');
    Route::get('/autoMainEquipData', 'ReportController@autoMainEquipData');

    Route::resource('jobs', 'JobController');
    Route::post('jobs/createTeam', 'JobController@createTeam')->name('jobs.createTeam');
    Route::post('jobs/updateTeam', 'JobController@updateTeam')->name('jobs.updateTeam');

    Route::get('/autoEquipData', 'ReportController@autoEquipData');

    Route::resource('jobTeams', 'JobTeamController');

    Route::get('histories', 'HistoryController@index')->name('histories.index');
    Route::post('histories/print', 'HistoryController@print')->name('histories.print');
});